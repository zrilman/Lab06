#pragma once
#include "FilteredBufferedArray.h"


template<class T>
class FilteredBufferedSet : public FilteredBufferedArray<T>
{
public:
	FilteredBufferedSet(std::function<bool(T)>);
	void add(const T&);
	void addAll(const FilteredBufferedArray<T>&);

};

template<class T>
FilteredBufferedSet<T>::FilteredBufferedSet(std::function<bool(T)> f) : FilteredBufferedArray(f) {}

template<class T>
void FilteredBufferedSet<T>::add(const T& info)
{
	auto temp = std::find(vec.begin(), vec.end(), info);
	if (temp == vec.end())
		if (f(info))
			vec.push_back(info);
}

template<class T>
void FilteredBufferedSet<T>::addAll(const FilteredBufferedArray<T>& other)
{
	for (auto temp : other.vec)
		if (f(temp))
			vec.push_back(temp);
}