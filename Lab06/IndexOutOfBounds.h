#pragma once
#include <exception>
#include <string>

class IndexOutOfBounds : public std::exception
{
public:
	IndexOutOfBounds(std::string ss) : msg(ss) {}
	virtual const char* what() const override
	{
		return "";
	}
private:
	std::string msg;
};
