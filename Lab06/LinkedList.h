#pragma once
#include <iostream>
#include "ITraversable.h"
#include <algorithm>
#include <list>

template<class T>
class LinkedList : public ITraversable<T>
{
public:
	LinkedList(std::function<bool(T)>) noexcept;
	~LinkedList() noexcept;

	void addFront(T) noexcept;
	void addBack(T) noexcept;
	T removeFront();
	T removeBack();
	T find(const T&) const;

	constexpr unsigned getSize() const noexcept;

	T operator[] (const int) const noexcept(false) override;
	void iterate() const noexcept(false) override;
private:
	std::list<T> list;
	std::function<bool(T)> f;
	template<class T>
	friend std::ostream&  operator<< (std::ostream&, const LinkedList<T>&);
};


template<class T>
LinkedList<T>::LinkedList(std::function<bool(T)> ff) noexcept : f(ff) {}

template<class T>
LinkedList<T>::~LinkedList() noexcept { list.erase(list.begin(), list.end()); }

template<class T>
void LinkedList<T>::addFront(T info) noexcept { list.push_front(info); }

template<class T>
void LinkedList<T>::addBack(T info) noexcept { list.push_back(info); }

template<class T>
T LinkedList<T>::removeBack()
{
	T toDelete = list.back();
	list.pop_back();
	return toDelete;
}

template<class T>
T LinkedList<T>::removeFront()
{
	T toDelete = list.front();
	list.pop_front();
	return toDelete;
}

template<class T>
T LinkedList<T>::find(const T& toFind) const
{
	auto& it = std::find(list.begin(), list.end(), toFind);
	return *it;
}

template<class T>
T LinkedList<T>::operator[] (const int index) const noexcept(false)
{
	if (index > unsigned(list.size() - 1))
		throw IndexOutOfBounds("Position " + std::to_string(index) + " is out of bounds!\n");

	std::cout << "LinkedList[" << index << "] = ";
	auto temp = list.begin();
	for (int i = 0; i < index; ++i, ++temp);
	return *temp;
}

template<class T>
void LinkedList<T>::iterate() const noexcept(false)
{
	for (auto temp : list)
		if (f(temp))
			std::cout << temp << " ";
	std::cout << std::endl;
}

template<class T>
std::ostream& operator<< (std::ostream& os, const LinkedList<T>& listObject)
{
	os << "Linked list: ";
	for (auto temp : listObject.list)
		os << temp << " ";
	return os;
}

template <class T>
constexpr unsigned LinkedList<T>::getSize() const noexcept { return list.size(); }