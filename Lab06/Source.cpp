#include "LinkedList.h"
#include "FilteredBufferedArray.h"
#include "FilteredBufferedSet.h"

void main()
{
	int numElements, element;
	FilteredBufferedArray<int> v1([](int x) { return x % 2 == 0; });

	int flag = 1, i = 0;
	while (flag)
	{
		std::cout << ++i << ".element: ";
		std::cin >> element;
		try
		{
			v1.add(element);
		}
		catch (std::exception& ex)
		{
			flag = 0;
		}
	}
	std::cout << "=========================" << std::endl;
	std::cout << v1;
	std::cout << "=========================" << std::endl;
	v1.set([](int x) { return x % 5 == 0; });
	std::cout << v1;
	std::cout << "=========================" << std::endl;
	flag = 1;
	while (flag)
	{
		i = v1.getSize()+1;
		std::cout << i++ << ".element: ";
		std::cin >> element;
		try
		{
			v1.add(element);
		}

		catch (std::exception& ex)
		{
			flag = 0;
		}
	}
	std::cout << "=========================" << std::endl;
	std::cout << v1;
	std::cout << "=========================" << std::endl;

	std::cin.ignore();
	std::cin.get();
}