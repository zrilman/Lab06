#pragma once
#include "ITraversable.h"
#include "IndexOutOfBounds.h"
#include <iostream>
#include <vector>


template<class T>
class FilteredBufferedArray :public ITraversable<T>
{
public:
	FilteredBufferedArray(std::function<bool(T)>) noexcept;
	~FilteredBufferedArray() noexcept;

	void add(const T) noexcept(false);
	void append(FilteredBufferedArray&);
	T operator[] (const int) const noexcept(false) override;
	void iterate() const noexcept(false) override;
	void set(std::function<bool(T)>);
	int getSize() const;
protected:
	std::vector<T> vec;
	std::function<bool(T)> f;

	template<class T>
	friend std::ostream& operator<< (std::ostream&, const FilteredBufferedArray<T>&);
};

template<class T>
FilteredBufferedArray<T>::FilteredBufferedArray(std::function<bool(T)> ff) noexcept : f(ff) {}

template<class T>
FilteredBufferedArray<T>::~FilteredBufferedArray() noexcept { vec.erase(vec.begin(), vec.end()); }

template<class T>
void FilteredBufferedArray<T>::add(const T data) noexcept(false)
{
	if (f(data))
		vec.push_back(data);
	else
		throw std::exception();
}

template<class T>
T FilteredBufferedArray<T>::operator[] (const int index) const noexcept(false)
{
	if (index > unsigned(vec.size() - 1))
		throw IndexOutOfBounds("Position " + std::to_string(index) + " out of bounds.\n");
	std::cout << "vec[" << index << "] = ";
	return vec[index];
}

template<class T>
void FilteredBufferedArray<T>::iterate() const noexcept(false)
{
	for (auto temp : vec)
		if (f(temp))
			std::cout << temp << " ";
	std::cout << std::endl;
}

template<class T>
void FilteredBufferedArray<T>::append(FilteredBufferedArray<T>& fba)
{
	int size = vec.size() - 1;
	for (auto temp : fba.vec)
		if (f(temp))
			vec.push_back(temp);

	auto temp = vec.begin();
	for (int i = 0; i <= size; ++i)
		if (fba.f(*temp))
		{
			fba.vec.push_back(*temp);
			temp++;
		}

}

template<class T>
void FilteredBufferedArray<T>::set(std::function<bool(T)> ff)
{
	int size = vec.size() - 1;
	f = ff;
	std::vector<T> temp;
	for (int i = 0; i <= size; i++)
		if (f(vec[i]))
			temp.push_back(vec[i]);
	vec = temp;
}

template<class T>
int FilteredBufferedArray<T>::getSize() const
{
	return vec.size();
}
template<class T>
std::ostream& operator<< (std::ostream& os, const FilteredBufferedArray<T>& fba)
{
	int i;
	os << "{ ";
	for ( i = 0; i < fba.vec.size()-1; ++i)
		os << fba.vec[i] << ",";

	os << fba.vec[i]<<" }" << std::endl;
	return os;
}