#pragma once
#include <functional>
#include <algorithm>

template<class T>
class ITraversable
{
public:
	virtual T operator[] (const int) const = 0;
	virtual void iterate() const = 0;
};